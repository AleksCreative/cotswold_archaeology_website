<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */

get_header('company'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main company" role="main">
                        <div id="panel-of-four" class="company-info ">
                                                <div class="one-fourth ">
                                                    <div class="services-panel" ><h4><?php the_field('title01'); ?></h4> </div>                              
                                                    <div class="panel-image"><?php if( get_field('image01') ): ?> 
                                                        <img src="<?php the_field('image01'); ?>" />
                                                        <?php endif; ?></div>
                                                    
                                                 </div>       
                                                <div class="one-fourth ">
                                                    <div class="services-panel"><h4 ><?php the_field('title02'); ?></h4> </div>                              

                                                        <div class="panel-image "><?php if( get_field('image02') ): ?> 
                                                        <img src="<?php the_field('image02'); ?>" />
                                                        <?php endif; ?></div>
                                                    
                                                </div>
                                                <div class="one-fourth ">
                                                    <div class="services-panel"><h4 ><?php the_field('title03'); ?></h4> </div>                              

                                                        <div class="panel-image "><?php if( get_field('image03') ): ?> 
                                                        <img src="<?php the_field('image03'); ?>" />
                                                        <?php endif; ?></div>
                                                    
                                                 </div> 
                                                <div class="one-fourth ">
                                                    <div class="services-panel"><h4 ><?php the_field('title04'); ?></h4> </div>                              

                                                        <div class="panel-image "><?php if( get_field('image04') ): ?> 
                                                        <img src="<?php the_field('image04'); ?>" />
                                                        <?php endif; ?></div>
                                                    
                                                 </div> 
                                                
                        </div>
                    
                    <div class="clearfix"></div>
                    <div class="services-background">
                        
                        <div class="fullpage main-left">
                               <div ><?php the_field('text-area01'); ?></div>
                               <div ><?php the_field('text-area02'); ?></div>
                    
                        </div><!--company-main-->
                                <div class="fullpage main-right">                                                
                                   <div ><?php the_field('text-area03'); ?></div>
                                   <div ><?php the_field('text-area04'); ?></div>         

                                </div><!--main-right panel-->
                                <div class="clearfix"></div>
                    </div>
                    <div><?php the_field('text-area-bottom'); ?></div>
                    
                    </main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('company');