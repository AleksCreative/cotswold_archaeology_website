<?php

/**
 * The template for displaying archive pages for Vacancies
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */

get_header('careers'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main careers" role="main">
                    <div class="two-third-left fullpage">
                        <?php
                       //1 Arguments for Current vacancies title
                                $args = array(
                                    
                                    'page_id'   => '10655',
                                    
                                );
                                /* The 1st Query (without global var) */
                                $query1 = new WP_Query( $args );
                                
                                // The 1st Loop
                                if ( $query1->have_posts ()) {
                                while ( $query1->have_posts() ) {
                                    $query1->the_post();
                                    echo '<h1>' . get_the_title() .
                                            '</h1>';
                                     
                                    
                                    
                                }

                                // Restore original Post Data
                                wp_reset_postdata(); } ?>
                                                
                        <?php
                       //2 Arguments for displaying current vacancies list
                                $args2 = array(
                                    'posts_per_page' => 20,
                                    'post_type'   => 'vacancy',
                                    
                                    
                                );
                                // The Query for vacancies list
                                $query2 = new WP_Query( $args2 );

                                // The 2nd Loop
                                if ( $query2->have_posts ()) {
                                while ( $query2->have_posts() ) {
                                    $query2->the_post();
                                    echo '<h5 class="home-link">-&nbsp;&nbsp;<a href="' . get_the_permalink() . '">' . get_the_title() .
                                            '</a></h5>';
                                    echo '<p>' . get_the_excerpt() . '</p>';
                                    
                                    
                                }
                               

                                /* Restore original Post Data 
                                 * NB: Because we are using new WP_Query we aren't stomping on the 
                                 * original $wp_query and it does not need to be reset with 
                                 * wp_reset_query(). We just need to set the post data back up with
                                 * wp_reset_postdata().
                                 */
                                wp_reset_postdata();

                                } 

                                ?>
                        <?php
                        // Arguments for the text printed from page /vac
                                $args3 = array(
                                    
                                    'page_id'   => '10655',
                                    
                                );
                                /* The 1st Query (without global var) */
                                $query3 = new WP_Query( $args3 );

                                // The  Loop
                                while ( $query3->have_posts() ) {
                                    $query3->the_post();
                                    
                                    echo '<p>' . the_content() .
                                            '</p>';
                                         
                                    
                                }

                                // Restore original Post Data
                                wp_reset_postdata(); ?></div><!-- .two-third-left -->
                   
                            
                         <div class="one-third-right fullpage">
                                                    
                                                        
                               <?php
                              // Arguments for vacancies list in sidebar
                                $args4 = array(
                                    'posts_per_page' => 20,
                                    'post_type'   => 'vacancy',
                                    
                                );
                                /* The 4nd Query (without global var) */
                                $query4 = new WP_Query( $args4 );

                                // The 3nd Loop
                                while ( $query4->have_posts() ) {
                                    $query4->the_post();
                                    
                                    echo '<div class="newsfeed-list"><hr><ul><li class="home-link newsfeed">               
                                    <div><h4 class="newsfeed"><a class="newsfeed" href="' . get_the_permalink() . '">' . get_the_title() .
                                            '</a></h4></div></li></ul></div>';
                                         
                                    
                                }

                                // Restore original Post Data
                                wp_reset_postdata(); ?>
                                                      
                        </div>
                                                    
                                    
                        <div class="clearfix"></div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('careers');
