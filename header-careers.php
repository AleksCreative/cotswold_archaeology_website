<?php
/**
 * The header for the Outreach sections of the Web site
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CA_Responsive_website
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ca_responsive_website' ); ?></a>

	<header id="masthead" class="site-header careers-site" >
         <nav id="top-navigation" class="menu-horizontal-top">
             <?php wp_nav_menu( array( 'theme_location' => 'top-menu', 'container' => '' ) ); ?>
         </nav><!-- #top-navigation -->
           <div class="header-wrapper ">     
            <div class="header-area top-30">
                    
                    <div class="site-branding">
                            <?php                        if (function_exists('the_custom_logo')) {
                            the_custom_logo();
                        }
                        ?>
                            <?php
                            if ( is_front_page() && is_home() ) : ?>
                                    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                            <?php else : ?>
                                    <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                            <?php
                            endif;

                            $description = get_bloginfo( 'description', 'display' );
                            if ( $description || is_customize_preview() ) : ?>
                                    <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
                            <?php
                            endif; ?>
                    </div><!-- site-branding -->                    
            </div><!-- header-area -->
           </div><!-- header-wrapper -->
         </header><!-- #masthead -->
         <div class="header-wrapper-menus careers-menu">
             
         <nav id="careers-menu" class="menu-horizontal" >
                                 <?php wp_nav_menu( array( 
                                    'theme_location' => 'careers-menu',  
                                    'menu_class' => 'nav-menu'
                                      ) ); ?>
             <div class="search-main"><?php get_search_form(); ?></div>
         </nav><!-- #careers menu -->
         <nav id="site-navigation-mobile" class="main-navigation menu-horizontal toggle-menu" >
                            
             <div>
                 <button class="menu-toggle">Menu &#9662;</button></div>
                             
                            <?php wp_nav_menu( array( 
                                    'theme_location' => 'careers-menu-mobile',                                                                       
                                    'menu_class' => 'nav-menu-mobile' ) ); ?>
         </nav>  <!-- mobile careers menu -->   
          </div><!--header wrapper menus-->
         
	

	<div id="content" class="site-content careers-pages-background">



