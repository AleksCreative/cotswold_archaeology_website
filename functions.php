<?php
/**
 * CA Responsive website's functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ca_responsive_website
 */

/**
 * Sets the maximum content width
 */
if (!isset($content_width)) {
    $content_width = 800;
} /* pixels */


/**
 * Adds ACF options page (used for the footer with new offices)
 */

if( function_exists('acf_add_options_page') ) {



	acf_add_options_page(array(
		'page_title' 	=> 'Contact office adresses',
		'menu_title'	=> 'Contact Settings',
		'menu_slug' 	=> 'contact-home-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

if ( ! function_exists( 'ca_responsive_website_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ca_responsive_website_setup() {
	/*
	 * Make theme available for translation.
	 */
	load_theme_textdomain( 'ca_responsive_website', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.

        add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for post thumbnails and featured images.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'ca_responsive_website' ),
                'menu-1-mobile' => esc_html__( 'Primary Mobile', 'ca_responsive_website' ),

	) );


/**
 * Function register additional menus.
 */
function register_additional_menus () {
    register_nav_menus(
            array(
                'top-menu' => esc_html( 'Top Menu' ),
                'company-menu' => esc_html( 'Company Menu'),
                'company-menu-mobile' => esc_html( 'Company Menu Mobile' ),
                'outreach-menu' => esc_html( 'Community Menu'),
                'outreach-menu-mobile' => esc_html( 'Community Menu Mobile'),
                'careers-menu' => esc_html( 'Careers Menu'),
                'careers-menu-mobile' => esc_html( 'Careers Menu Mobile'),
                'contact-menu' => esc_html( 'Contact Menu'),
                'contact-menu-mobile' => esc_html( 'Contact Menu Mobile'),
               )
            );
}
add_action( 'init', 'register_additional_menus');



function define_class ($args) {/*== Set classes of menu container ==*/
    $args['container_class'] = str_replace(' ','-',$args['theme_location']).'-container'; return $args;
}
add_filter ('wp_nav_menu_args', 'define_class');


	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

        // Adds post formats support.

        add_theme_support( 'post-formats',  array (
                'aside',
                'gallery',
                'quote',
                'image',
                'video',
        ) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ca_responsive_website_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

        add_theme_support( 'custom-logo', array (
                'width'       => 350,
                'height'      => 150,
                'flex-width'  => true,
                'flex-height' => true,
                'header-text' => '',
            ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'ca_responsive_website_setup' );

function ca_responsive_website_fonts_url() {
	$fonts_url = '';

	/**
	 * Translators: If there are characters in your language that are not
	 * supported by Lato and Playfair Display, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$lato = _x( 'on', 'Lato font: on or off', 'ca_responsive_website' );
        $playfair_display = _x( 'on', 'Playfair Display font: on or off', 'ca_responsive_website' );

        $font_families = array();

	if ( 'off' !== $lato ) {
		$font_families[] = 'Lato:400,400i,700,700i';
        }
        if ( 'off' !== $playfair_display ) {
		$font_families[] = 'Playfair Display:400,400i';
        }
        if ( in_array( 'on', array($lato, $playfair_display)) ){
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function ca_responsive_website_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'ca_responsive_website-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'ca_responsive_website_resource_hints', 10, 2 );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ca_responsive_website_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ca_responsive_website_content_width', 800 );
}
add_action( 'after_setup_theme', 'ca_responsive_website_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

/* Changing excerpt more */
   function new_excerpt_more($more) {
   global $post;
   return '… <a id="read-more" href="'. get_permalink($post->ID) . '">' . '<br>Read More &raquo;' . '</a>';
   }
   add_filter('excerpt_more', 'new_excerpt_more');



   function ca_responsive_website_widgets_init() {


         register_sidebar( array(
		'name'          => esc_html__( 'Home page new content', 'ca_responsive_website' ),
		'id'            => 'new-content',
		'description'   => esc_html__( 'Add new content here.', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
         register_sidebar( array(
		'name'          => esc_html__( 'CA Events', 'ca_responsive_website' ),
		'id'            => 'ca-events',
		'description'   => esc_html__( 'Add content here.', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title latest-news-title">',
		'after_title'   => '</h4>',
	) );

        register_sidebar( array(
		'name'          => esc_html__( 'Andover footer office location', 'ca_responsive_website' ),
		'id'            => 'office-locations-andover',
		'description'   => esc_html__( 'Footer area sidebar Andover', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Cirencester footer office locations', 'ca_responsive_website' ),
		'id'            => 'office-locations-cirencester',
		'description'   => esc_html__( 'Footer area sidebar Cirencester', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Exeter footer office locations', 'ca_responsive_website' ),
		'id'            => 'office-locations-exeter',
		'description'   => esc_html__( 'Footer area sidebar Exeter', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title ">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'MK footer office locations', 'ca_responsive_website' ),
		'id'            => 'office-locations-mk',
		'description'   => esc_html__( 'Footer area sidebar MK', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title ">',
		'after_title'   => '</h2>',
	) );
            register_sidebar( array(
		'name'          => esc_html__( 'Footer quick-menu Company', 'ca_responsive_website' ),
		'id'            => 'footer-menu-company',
		'description'   => esc_html__( 'Footer area quick menu for Company and Services', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
            register_sidebar( array(
		'name'          => esc_html__( 'Footer quick-menu Community', 'ca_responsive_website' ),
		'id'            => 'footer-menu-community',
		'description'   => esc_html__( 'Footer area quick menu for Community and Research', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
            register_sidebar( array(
		'name'          => esc_html__( 'Footer quick-menu Careers', 'ca_responsive_website' ),
		'id'            => 'footer-menu-careers',
		'description'   => esc_html__( 'Footer area quick menu for Careers', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
            register_sidebar( array(
		'name'          => esc_html__( 'Footer Contact Form', 'ca_responsive_website' ),
		'id'            => 'footer-contact-form',
		'description'   => esc_html__( 'Footer area contact form', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
            register_sidebar( array(
		'name'          => esc_html__( 'Footer site map', 'ca_responsive_website' ),
		'id'            => 'footer-site-map',
		'description'   => esc_html__( 'Footer area quick menu for Company and Services', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'News sidebar', 'ca_responsive_website' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( 'Single event sidebar', 'ca_responsive_website' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
        register_sidebar( array(
		'name'          => esc_html__( '404 navigation', 'ca_responsive_website' ),
		'id'            => 'sidebar-404',
		'description'   => esc_html__( 'Add menu here.', 'ca_responsive_website' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ca_responsive_website_widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function ca_responsive_website_scripts() {


        // Enquee Google fonts: Lato and PT Serif Caption
        wp_enqueue_style( 'ca_responsive_website-fonts', ca_responsive_website_fonts_url() );

	wp_enqueue_style( 'ca_responsive_website-style', get_stylesheet_uri() );

        wp_enqueue_script( 'ca_responsive_website-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), '20160909', true );

        wp_enqueue_script( 'ca_responsive_website-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ca_responsive_website_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
