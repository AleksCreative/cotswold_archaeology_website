<?php
/**
 * Template Name: Company pages
 *
 *
 */

get_header('company'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main company" role="main">
                    <div class="two-third-left fullpage">
                        <div><div class="breadcrumb"><?php get_breadcrumb(); ?></div></div>
                                        <?php
                                        while ( have_posts() ) : the_post();

                                                get_template_part( 'template-parts/content', 'page' );

                                                // If comments are open or we have at least one comment, load up the comment template.
                                                if ( comments_open() || get_comments_number() ) :
                                                        comments_template();
                                                endif;

                                        endwhile; // End of the loop.
                                        ?>
                                    </div>
                                   
                                    <div class="one-third-right fullpage">
                                                    <div class="page-navi">
                                                        
                                                      <?php
                                                            $children = wp_list_pages( 'title_li=&child_of='.$post->ID.'&echo=0' );
                                                            if ( $children) : ?>
                                                                <h5> <?php echo get_the_title(); ?></h5>
                                                                <ul>
                                                                    <?php echo $children; ?>
                                                                </ul>
                                                            <?php endif; ?>
                                                      
                                                    </div>
                                                    
                                    </div>
                                    <div class="clearfix"></div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('company');