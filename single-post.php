<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main class="site-main" role="main">
                    <div class="mobile-container">
                    <div class="two-third  main-news">
                        
                        <?php
                        if ( have_posts() ) :

                                if ( is_home() && ! is_front_page() ) : ?>
                                        <header>
                                                <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                                        </header>

                                <?php
                                endif;

                                /* Start the Loop */
                                while ( have_posts() ) : the_post();

                                        /*
                                         * Include the Post-Format-specific template for the content.
                                         * If you want to override this in a child theme, then include a file
                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                         */
                                        get_template_part( 'template-parts/content', get_post_format() );
                                        echo '<hr>';

                                endwhile;
                                
                                the_posts_navigation();

                        else :

                                get_template_part( 'template-parts/content', 'none' );

                        endif;
                        
                        ?>
                    </div>
                    <div class="one-third  sidebar-news">
                        <div class="category-list">
                          <?php  wp_list_categories( array ( 'echo' ) ); ?>
                        </div><br>
                        <!--<div class="category-list">
                            <li class="categories">Browse by County:
                                <ul><li class="cat-item">Gloucestershire</li>
                                <li class="cat-item">Oxfordshire</li>
                                <li class="cat-item">etc. (under development)</li></ul></li>
                        </div>-->
                        <div>
                        <?php dynamic_sidebar( 'sidebar-1' ); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    </div><!-- mobile-container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
