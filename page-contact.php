<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */

get_header('contact'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main contact" role="main">
                    <div><h1 ><?php the_field('general_enquires'); ?></h1></div>
                    <div><?php the_field('general_enquires_text'); ?></div>
                     <div class="clearfix"></div>
										 <div class="office-flex-container ">

                         <?php

                         // check if the repeater field has rows of data
                         if( have_rows('footer-office', 'option') ): // loop through the rows of data ?>


                             <?php while ( have_rows('footer-office', 'option') ) : the_row(); ?>
															 <div class="office-divider"></div>
                               <div class="office-locations flex-contact-office-locations">
																	 <div class="office-details">
		                               <h3><a href="<?php the_sub_field('footer_office_url') ?>"><?php  // display a sub field value
		                               the_sub_field('footer_office_name'); ?></a></h3>
		                               <?php the_sub_field('footer_office_address'); ?>
                                   <br><strong><?php the_sub_field('footer_office_phone_number'); ?></strong></div>
																	 <div class="office-map"><?php the_sub_field('google_map'); ?></div>
																	 <div class="office-main-contacts"><?php the_sub_field('key_contacts'); ?></div>
                               </div>
															 <?php
															 	if( have_rows('office_head')):
																	while ( have_rows('office_head')) : the_row(); ?>
															 <div class="office-head padding10 flex-contact-office-locations contact-pages-background">
																 <div class="padding10 office-head-photo"><img src="<?php the_sub_field('office_head_photo'); ?>" /></div>
																 <div class="office-head-name">
																	 <h4> <?php the_sub_field('office_head_name'); ?></h4><strong>Office Head</strong>
																	 <p>email: <strong><a href="mailto:<?php the_sub_field('office_head_email'); ?>"><?php the_sub_field('office_head_email'); ?></a></strong></p>
																	  <p>phone: <strong><?php the_sub_field('office_head_phone'); ?></strong></p>


																 </div>
															 </div>
														 <?php endwhile ?>



													<?php else :



															// no rows found



													endif;



													?>

                            <?php endwhile ?>

                         <?php else :

                             // no rows found

                         endif;

                         ?>

                     </div>


                  <!--  <div class="panel-of-four" >
                        <div class="one-fourth padding10" >
                            <div ><h4><?php the_field('office_1_title'); ?></h4> </div>

                                <div><?php the_field('office_1_address'); ?></div>
                         </div>
                       <div class="one-fourth padding10" >
                            <div ><h4 ><?php the_field('office_2_title'); ?></h4> </div>

                                <div><?php the_field('office_2_address'); ?></div>
                         </div>
                         <div class="one-fourth padding10" >
                            <div ><h4 ><?php the_field('office_3_title'); ?></h4> </div>

                                <div><?php the_field('office_3_address'); ?></div>
                         </div>
                        <div class="one-fourth padding10" >
                            <div ><h4 ><?php the_field('office_4_title'); ?></h4> </div>

                                <div><?php the_field('office_4_address'); ?></div>
                         </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div><?php the_field('additional_text'); ?></div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('contact');
