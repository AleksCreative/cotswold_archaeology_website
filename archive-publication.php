<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */


  get_header( 'community' );
  ?>

	<div id="primary" class="content-area">
		<main class="site-main community" role="main">
                  <div class="mobile-container">
                      <div class="two-third"><br>

                       <?php

                       the_archive_description( '<div class="taxonomy-description">', '</div>' );

                       ?>
                       <div class="padding-left20">


 <?php
                        if ( have_posts() ) :
                                /* Start the Loop */
                                while ( have_posts() ) : the_post(); ?>

                           <div class="fullpage one-third-left height-150" ><?php the_post_thumbnail ();?></div>
                                <div class="two-third-right fullpage">
                                 <?php the_title('<h2 class="entry-title pub-list-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>'); ?>
                                 <p><strong><?php the_field('book_title'); ?></strong></p>
                                 <p><?php the_field('group_description'); ?></p>
                                <!-- <?php the_excerpt() ; ?> -->
                                </div>

                               <div class="clearfix"></div>

                                <?php endwhile;

                                the_posts_pagination();
                                ?>


                      <?php  endif;

                        ?>
                               <br>
                     </div>


                        </div>
                         <div class="one-third publication-list">
                        <div class="category-list community">
                            <h6>CA Publications</h6>
                          <?php

                        $taxonomy = 'publication_category';
                        $terms = get_terms($taxonomy); // Get all terms of a taxonomy

                        if ( $terms && !is_wp_error( $terms ) ) :
                        ?>
                            <ul>
                                <?php foreach ( $terms as $term ) { ?>
                                    <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php endif;?>

                        </div>

                    </div>
                    <div class="clearfix"></div>
                    </div><!-- mobile-container -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('community'); 
