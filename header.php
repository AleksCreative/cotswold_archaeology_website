<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CA_Responsive_website
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ca_responsive_website' ); ?></a>

	<header id="masthead" class="site-header home-site" >
            
            
              
                        <div class="site-branding"> 
                                <?php                    if (function_exists('the_custom_logo')) {
                                the_custom_logo();
                            }
                            ?>

                                <?php
                                if ( is_front_page() && is_home() ) : ?>
                                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                                <?php else : ?>
                                        <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                                <?php
                                endif;

                                $description = get_bloginfo( 'description', 'display' );
                                if ( $description || is_customize_preview() ) : ?>
                                        <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
                                <?php
                                endif; ?>
                        </div><!-- .site-branding -->
                        <div class="header-wrapper ">
                        <nav id="site-navigation" class="main-navigation menu-horizontal">                                   
                            <?php wp_nav_menu( array( 
                                    'theme_location' => 'menu-1',
                                    'menu_id' => 'primary-menu',
                                    'menu_class' => 'nav-menu' ) ); ?>
                                 
                            <div class="search-main"><?php get_search_form(); ?></div>
                            
                            
                        </nav><!-- #site-navigation -->
                        
                </div><!--.header wrapper-->
                        </header><!-- #masthead -->
                        <nav id="site-navigation-mobile" class="main-navigation menu-horizontal toggle-menu">
                <div class="button-panel">
                                    <button class="menu-toggle">Menu &#9662;</button>
                                </div> 
                        <?php wp_nav_menu( array( 
                                    'theme_location' => 'menu-1-mobile',                                    
                                    'menu_class' => 'nav-menu-mobile' ) ); ?>
                        </nav>
                
	

	<div id="content" class="site-content front-page-background">
