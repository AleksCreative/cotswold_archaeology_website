<?php

/**
 * The template for displaying archive pages for Vacancies
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */

get_header('careers'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main careers" role="main">
                    <div class="two-third-left fullpage">
                                      <div><?php

                                            if(get_field('job_title'))
                                            {
                                                    echo '<h2>' . get_field('job_title') . '</h2>';
                                            }

                                            ?>  </div>
                        <div><?php

                                            if(get_field('office_vacancy'))
                                            {
                                                    echo '<strong>' . get_field('office_vacancy') . '</strong>';
                                            }

                                            ?>  </div>
                         <div><?php

                                            if(get_field('salary_vacancy'))
                                            {
                                                    echo '<strong>' . get_field('salary_vacancy') . '</strong>';
                                            }

                                            ?>  </div>
                         <div><?php

                                            if(get_field('contract_type'))
                                            {
                                                    echo '<strong>' . get_field('contract_type') . '</strong>';
                                            }

                                            ?>  </div>
                         <div><?php

                                            if(get_field('job_description'))
                                            {
                                                    echo '<p>' . get_field('job_description') . '</p>';
                                            }

                                            ?>  </div>
                            
                                    </div>
                                   
                                    <div class="one-third-right fullpage">
                                                    <div >
                                                        
                                                      <?php
                                                          // Arguments for post list
                                $args3 = array(
                                    'posts_per_page' => 20,
                                    'post_type'   => 'vacancy',
                                    
                                );
                                /* The 3nd Query (without global var) */
                                $query3 = new WP_Query( $args3 );

                                // The 3nd Loop
                                while ( $query3->have_posts() ) {
                                    $query3->the_post();
                                    
                                    echo '<div class="newsfeed-list"><hr><ul><li class="home-link newsfeed">               
                                    <div><h4 class="newsfeed"><a class="newsfeed" href="' . get_the_permalink() . '">' . get_the_title() .
                                            '</a></h4></div></li></ul></div>';
                                         
                                    
                                }

                                // Restore original Post Data
                                wp_reset_postdata(); ?>
                                                      
                                                    </div>
                                                    
                                    </div>
                                    <div class="clearfix"></div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('careers');
