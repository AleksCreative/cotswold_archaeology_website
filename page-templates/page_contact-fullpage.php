<?php
/**
 * Template Name: Contact pages
 *
 *
 */

get_header('contact'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main contact" role="main">
                    <div class="mobile-container">
                                    <div class="fullpage">
                                        <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
                                        <?php
                                        while ( have_posts() ) : the_post();

                                                get_template_part( 'template-parts/content', 'page' );

                                                // If comments are open or we have at least one comment, load up the comment template.
                                                if ( comments_open() || get_comments_number() ) :
                                                        comments_template();
                                                endif;

                                        endwhile; // End of the loop.
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>
                    </div><!-- mobile-container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('contact');
