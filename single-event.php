<?php
/**
 * Template Name: Single event page
 *
 *
 */

get_header('community'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main community" role="main">
                     <div class="mobile-container">
                                    <div class="two-third-left fullpage">
                                        <?php
                                        while ( have_posts() ) : the_post();

                                                get_template_part( 'template-parts/content', 'page' );

                                                // If comments are open or we have at least one comment, load up the comment template.
                                                if ( comments_open() || get_comments_number() ) :
                                                        comments_template();
                                                endif;

                                        endwhile; // End of the loop.
                                        ?>
                                    </div>
                                   
                                    <div class="one-third-right fullpage">
                                                    <div class="page-navi">
                                                        
                                                      <div>
                        <?php dynamic_sidebar( 'sidebar-2' ); ?>
                        </div>
                                                      
                                                    </div>
                                                    
                                    </div>
                                    <div class="clearfix"></div>
                    </div><!-- mobile-container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('community');
