<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */

get_header('community'); ?>

	<div id="primary" class="content-area">
            <main id="main" class="site-main community" role="main">
                        <div id="outreach-main" class="fullpage main-left">

                            <div id="latest-news" class=" latest-news outreach-news grey-box " >

                                            <h2>Community news</h2>
                                             <?php
                                                // Arguments for first post
                                                $args = array(
                                                    'posts_per_page' => 1,
                                                    'cat' => 90
                                                );
                                                // The Query for first post
                                                $query1 = new WP_Query( $args );

                                                // The Loop
                                                while ( $query1->have_posts() ) :
                                                    $query1->the_post(); ?>
                                            <?php the_title('<h4 class="home-link"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>');?>
                            <div><?php the_post_thumbnail ();?></div>
                            <div><?php the_excerpt() ; ?></div>
                            <?php endwhile; ?>
                                            <?php

                                                /* Restore original Post Data
                                                 * NB: Because we are using new WP_Query we aren't stomping on the
                                                 * original $wp_query and it does not need to be reset with
                                                 * wp_reset_query(). We just need to set the post data back up with
                                                 * wp_reset_postdata().
                                                 */
                                                wp_reset_postdata(); ?>
                            <?php

                                                // Arguments for post list
                                                $args2 = array(
                                                    'posts_per_page' => 2,
                                                    'offset'=> 1,
                                                    'cat' => 90
                                                );
                                                /* The 2nd Query (without global var) */
                                                $query2 = new WP_Query( $args2 );

                                                // The 2nd Loop
                                                while ( $query2->have_posts() ) :
                                                    $query2->the_post(); ?>
<div class="newsfeed-list">
                                        <hr>
                                        <ul>
                                            <li class="home-link newsfeed">
                                                <div class="float-left"><?php the_post_thumbnail ();?></div>
                                                <div><?php the_title('<h4 class="newsfeed"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>'); ?></div>
                                                <div><?php custom_excerpt(200); ?></div>
                                            </li>
                                        </ul>
                                    </div>

                               <div class="clearfix"></div>

                                <?php endwhile; ?>

                               <?php

                                                // Restore original Post Data
                                                wp_reset_postdata();

                                                ?>


                                        </div><!-- latest news and content -->

                                       </div><!---.main-left panel--->



                        <div class="main-right fullpage">
                            <div class="grey-box outreach-events events-box half">
                                <h2>Events</h2>
                                <?php dynamic_sidebar( 'ca-events' ); ?><div class="clearfix"></div>
                            </div>
                            <div class="latest-news outreach-news grey-box ">
                                <h2>Latest Publications</h2>
                                <div><br>
                                <?php
                                                // Arguments for  publications post
                                                $args3 = array(
                                                    'posts_per_page' => 3,
                                                    'post_type'   => 'publication',
                                                );
                                                // The Query for publications post
                                                $query3 = new WP_Query( $args3 );

                                                // The Loop
                                                while ( $query3->have_posts() ) :
                                                    $query3->the_post(); ?>
                                     <div class="fullpage one-third-left " ><?php the_post_thumbnail ();?></div>
                                <div class="two-third-right fullpage"><?php the_title('<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>') ?>
                                <strong><?php the_field('book_title'); ?></strong></div>
                               <div class="clearfix"></div><hr class="com"/>

                                <?php endwhile; ?>
                                                <?php
                                                /* Restore original Post Data
                                                 * NB: Because we are using new WP_Query we aren't stomping on the
                                                 * original $wp_query and it does not need to be reset with
                                                 * wp_reset_query(). We just need to set the post data back up with
                                                 * wp_reset_postdata().
                                                 */
                                                wp_reset_postdata();

                                                ?>

                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div> <!---.main-right panel--->

											  <div class="clearfix"></div>

                    <div  class="outreach-info panel-of-four">
                        <div class="one-fourth outreach-p1-color">
                            <div class="panel-header"><h4 ><?php the_field('cpanel_1_title'); ?></h4> </div>

                                <div class="panel-image "><?php if( get_field('cpanel_1_image') ): ?>
                                <img src="<?php the_field('cpanel_1_image'); ?>" />
                                <?php endif; ?></div>
                         </div>
                        <div class="one-fourth outreach-p2-color">
                            <div class="panel-header"><h4 ><?php the_field('cpanel_2_title'); ?></h4> </div>

                                <div class="panel-image "><?php if( get_field('cpanel_2_image') ): ?>
                                <img src="<?php the_field('cpanel_2_image'); ?>" />
                                <?php endif; ?></div>
                         </div>
                         <div class="one-fourth outreach-p3-color">
                             <div class="panel-header"><h4 ><?php the_field('cpanel_3_title'); ?></h4></div>

                                <div class="panel-image"><?php if( get_field('cpanel_3_image') ): ?>
                                <img src="<?php the_field('cpanel_3_image'); ?>" />
                                <?php endif; ?></div>
                                     </div>
                        <div class="one-fourth outreach-p4-color">
                            <div class="panel-header"><h4 ><?php the_field('cpanel_4_title'); ?></h4></div>

                                <div class="panel-image "> <?php if( get_field('cpanel_4_image') ): ?>
                                <img src="<?php the_field('cpanel_4_image'); ?>" />
                                <?php endif; ?></div>
                                </div>
                    </div>
                    <div class="clearfix"></div>
										<div class="community-pages-background padding1020">


											<?php
												the_content(); ?>

										</div>
                    </main><!-- #main -->

	</div><!-- #primary -->

<?php

get_footer('community');
