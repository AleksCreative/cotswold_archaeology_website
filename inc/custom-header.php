<?php
/**
 * Sample implementation of the Custom Header feature.
 *
 * You can add an optional custom header image to header.php like so ...
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package Theme Name
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses ca_responsive_website_header_style()
 */
function ca_responsive_website_custom_header_setup() {
    add_theme_support( 'custom-header', apply_filters( 'ca_responsive_website_custom_header_args', array(
        'default-image'          => '',
        'default-text-color'     => '000000',
        'width'                  => 1920,
        'height'                 => 325,
        'flex-height'            => false,
        'wp-head-callback'       => 'ca_responsive_website_header_style',
    ) ) );
}
add_action( 'after_setup_theme', 'ca_responsive_website_custom_header_setup' );

if ( ! function_exists( 'ca_responsive_website_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog.
 *
 * @see ca_responsive_website_custom_header_setup().
 */
function ca_responsive_website_header_style() {
    $header_text_color = get_header_textcolor();
    $header_image = get_header_image();

    if ( $header_image ) : ?>
        <style type="text/css">
            .site-header {
                position: relative;
            }
            .site-header:before {
                background-image: url( <?php echo esc_url( $header_image ); ?>);
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                content: "";
                display: block;
                height: 100%;
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                z-index: -1;
            }
        </style>
    <?php
    endif;

    /*
     * If no custom options for text are set, let's bail.
     * get_header_textcolor() options: Any hex value, 'blank' to hide text. Default: HEADER_TEXTCOLOR.
     */
    if ( HEADER_TEXTCOLOR === $header_text_color ) {
        return;
    }

    // If we get this far, we have custom styles. Let's do this.
    ?>
    <style type="text/css">
    <?php
        // Has the text been hidden?
        if ( ! display_header_text() ) :
    ?>
        .site-title,
        .site-description {
            position: absolute;
            clip: rect(1px, 1px, 1px, 1px);
        }
    <?php
        // If the user has set a custom color for the text use that.
        else :
    ?>
        .site-title a,
        .site-description {
            color: #<?php echo esc_attr( $header_text_color ); ?>;
        }
    <?php endif; ?>
    </style>
    <?php
}
endif;