<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CA_Responsive_website
 */

?>

	
    </div><!-- #content -->
	<footer id="colophon" class="site-footer" >
            
          
            <div class="company-footer">
                <div class="footer-sidebar site-footer footer-wrapper ">
		<div class="footer-sidebar site-info footer-stack3">
                        <p>Cotswold Archaeology Ltd. Registered in England</p>
                        <p>Reg no: 2362531</p>
			<p>Registered Charity No: 1001653</p>
                        <p> &copy; <?php echo date('Y'); ?> Cotswold Archaeology. All Rights Reserved </p>
		</div><!-- .site-info -->
                <div class="footer-site-map footer-stack3">
                        <aside id="footer-site-map">
                        <?php
                        if(is_active_sidebar('footer-site-map')){
                        dynamic_sidebar('footer-site-map');
                        }
                        ?>
                        </aside>
                </div>
                </div>
            </div>
        </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
