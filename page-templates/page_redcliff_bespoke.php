<?php
/**
 * Template Name: Redcliffe page
 *
 *
 */

get_header('redcliffe'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main redcliffe" role="main">
                     <div class="mobile-container">
                                    <div class="fullpage">
                                        <div class="breadcrumb"><?php get_breadcrumb(); ?></div>
                                       
                                        <?php
                                        while ( have_posts() ) : the_post();

                                                get_template_part( 'template-parts/content', 'page' );

                                                
                                                if ( comments_open() || get_comments_number() ) :
                                                        comments_template();
                                                endif;

                                        endwhile; 
                                        ?>
                                       
                                    </div>
                                    <div class="clearfix"></div>
                    </div><!-- mobile-container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('redcliffe');
