<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CA_Responsive_website
 */

?>

	
    </div><!-- #content -->
	<footer id="colophon" class="site-footer" >
            
            <div class="home-footer-1">   
                <div class="site-footer footer-wrapper ">
                     
                    
                    <div class="footer-flex-container ">
                       
                        <?php

                        // check if the repeater field has rows of data
                        if( have_rows('footer-office', 'option') ): // loop through the rows of data ?> 
                            
                                
                            <?php while ( have_rows('footer-office', 'option') ) : the_row(); ?>

                              <div class="office-locations flex-office-locations">
                              <h2><a href="<?php the_sub_field('footer_office_url') ?>"><?php  // display a sub field value
                              the_sub_field('footer_office_name'); ?></a></h2>
                              <?php the_sub_field('footer_office_address'); ?>
                                  <br><strong><?php the_sub_field('footer_office_phone_number'); ?></strong>
                              </div>

                           <?php endwhile ?>
                             
                        <?php else :

                            // no rows found

                        endif;

                        ?>
                    </div>
                       
                    
                    
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="home-footer-2">
                <div class="footer-sidebar site-footer footer-wrapper ">
                        <div class="footer-2 footer-stack">
                        <aside id="footer-menu-company">
                        <?php
                        if(is_active_sidebar('footer-menu-company')){
                        dynamic_sidebar('footer-menu-company');
                        }
                        ?>
                        </aside>
                        </div>
                    <div class="footer-2 footer-stack">
                        <aside id="footer-menu-community">
                        <?php
                        if(is_active_sidebar('footer-menu-community')){
                        dynamic_sidebar('footer-menu-community');
                        }
                        ?>
                        </aside>
                        </div>
                    <div class="footer-2 footer-stack">
                        <aside id="footer-menu-careers">
                        <?php
                        if(is_active_sidebar('footer-menu-careers')){
                        dynamic_sidebar('footer-menu-careers');
                        }
                        ?>
                        </aside>
                        </div>
                    <div class="footer-2 footer-stack">
                        <aside id="footer-contact-form">
                        <?php
                        if(is_active_sidebar('footer-contact-form')){
                        dynamic_sidebar('footer-contact-form');
                        }
                        ?>
                        </aside>
                        </div>
                </div> 
            </div>
            <div class="home-footer-3">
                <div class="footer-sidebar site-footer footer-wrapper ">
		<div class="footer-sidebar site-info footer-stack3">
                        <p>Cotswold Archaeology Ltd. Registered in England</p>
                        <p>Reg no: 2362531</p>
			<p>Registered Charity No: 1001653</p>
                        <p> &copy; <?php echo date('Y'); ?> Cotswold Archaeology. All Rights Reserved </p>
		</div><!-- .site-info -->
                <div class="footer-site-map footer-stack3">
                        <aside id="footer-site-map">
                        <?php
                        if(is_active_sidebar('footer-site-map')){
                        dynamic_sidebar('footer-site-map');
                        }
                        ?>
                        </aside>
                </div>
                </div>
            </div>
        </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
